package practice.weather.test;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import practice.weather.CityWeather;
import practice.weather.WeatherReport;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration
public class WeatherReportTest {

    @Before
    public void setUp() throws Exception {

    }
    
    @Configuration
    @ComponentScan(basePackages = {"weather"})
    static class TemplateRepositoryTestConfiguration {
    }
    
    @Test
    public void testGetWeatherReport() throws Exception {
    	CityWeather cityWeather = WeatherReport.getReport(85001);
        Assert.assertThat(cityWeather, CoreMatchers.notNullValue());

        Assert.assertThat(cityWeather.getCityName(), CoreMatchers.equalTo("Phoenix"));
    }
    
    @Test
    public void testGetWeatherReportForWrongZip() throws Exception {
    	CityWeather cityWeather = WeatherReport.getReport(02564);
        Assert.assertThat(cityWeather, CoreMatchers.nullValue());
    }
}
