package practice.weather;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Properties;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class WeatherReport {

	public static void main(String[] args) throws IOException {
		String input;
		try{
			System.out.println("Tomorrow predicted temperature");
			System.out.println("Enter the 5 digit Zip code of the city in USA");
			BufferedReader br =  new BufferedReader(new InputStreamReader(System.in));

			while((input=br.readLine())!=null && !input.equals("exit") && !input.equals("quit")) {
				try {
					int zipcode = 75252;
					zipcode = Integer.parseInt(input);
					CityWeather cityWeather = getReport(zipcode);
					
					//Finds the coolest hour of tomorrow for the city of user filled zipcode
					System.out.println("Coolest HOUR in " + cityWeather.getCityName() + " tomorrow on " + cityWeather.getDate() + " would be at " + cityWeather.getTime());
					System.out.println("and the temperature would be " + cityWeather.getTemp() +  " F");
					
				}catch (NumberFormatException e) {
					System.out.println("Please enter a valid zip code");
				}

				System.out.println("Want to check temperature at another location,\nthen please Enter the 5 digit Zip code of the city in USA or enter exit to quit");

			}
			System.out.println(".....Program Exited.....");

		}catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static CityWeather getReport(int zipcode) {

		try {
			// Make a URL to the web page
			// You can generate the APP_ID and APP_CODE after signing up on "https://developer.here.com" 
			// metric = false, Temperature would be in fahrenheit (For Celcius type metric = true)
			// reports back hourly forecast for the next 7 days

			
			Properties prop = new Properties();
			//load a properties file from class path, inside static method
			prop.load(WeatherReport.class.getClassLoader().getResourceAsStream("config.properties"));
			
			String apiUrl = "https://weather.api.here.com/weather/1.0/report.json?app_id="+prop.getProperty("app.id")+"&app_code="+ prop.getProperty("app.code")+"&product=forecast_hourly&metric=false&zipcode=" + zipcode;
			
			System.out.println("apiUrl ==> "+ apiUrl);
			URL url = new URL(apiUrl);
			// Get the input stream through URL Connection
			int responseCode = 0;

			HttpURLConnection httpConnection = (HttpURLConnection)url.openConnection();
			responseCode = httpConnection.getResponseCode();

			if(responseCode == 200) {
				InputStream is = httpConnection.getInputStream();
				BufferedReader ar = new BufferedReader(new InputStreamReader(is));
				String line = ar.readLine();
				JSONObject obj = new JSONObject(line);

				JSONArray arr = obj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation").getJSONArray("forecast");
				String cityName = obj.getJSONObject("hourlyForecasts").getJSONObject("forecastLocation").getString("city");

				ArrayList<Double>  temp = new ArrayList<Double>();
				ArrayList<String> dateTime = new ArrayList<String>();

				for (int i=0; i < 24; i++){
					temp.add(Double.parseDouble((arr.getJSONObject(i).getString("temperature"))));
					dateTime.add(arr.getJSONObject(i).getString("localTime"));
				}

				int minIndex = temp.indexOf(Collections.min(temp));
				String time = dateTime.get(minIndex).substring(0,2);
				String date = dateTime.get(minIndex).substring(2, dateTime.get(minIndex).length());
				date = date.substring(0,2) + "-" + date.substring(2,4) + "-" + date.substring(4,8);
				String meridiem = "am";
				//Display time in meridiem
				if(Integer.parseInt(time) > 11){
					if(time!="12")
						time = Integer.toString((Integer.parseInt(time) - 12));
					meridiem = "pm";
				}else{
					if(time=="00")
						time = "12";
				}		
	
				is.close();
				httpConnection.disconnect();
				ar.close();
				
				return new CityWeather(cityName, date, time + " " + meridiem, temp.get(minIndex));				
			}else if(responseCode == 403){   	
				System.out.println("Please try in few seconds");
			}else{
				System.out.println("City not found, please enter a valid Zip Code");
			}

		}catch (IOException e) {
			System.out.println("City not found, please enter a valid Zip Code");
		}catch (JSONException e) {
			System.out.println(" Invalid API format ");
		}
		
		return null;
	}
}


