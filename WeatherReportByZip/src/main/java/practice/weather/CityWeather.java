package practice.weather;

public class CityWeather {	
	
	private String cityName;
	private String date;
	private String time;
	private Double temp;
	
	public CityWeather(String cityName, String date, String time, Double temp) {
		this.cityName = cityName;
		this.date = date;
		this.time = time;
		this.temp = temp;
	}
	
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public Double getTemp() {
		return temp;
	}
	public void setTemp(Double temp) {
		this.temp = temp;
	}	
	

}
